import { css } from 'styled-components'

export const maxContentWidth = {
  mediumScreen: '750px',
  ngFrameSize: '875px',
  largeScreen: '970px',
};

export const viewSizes = {
  verySmallScreen: 350,
  smallScreen: 600,
  mediumScreen: 769,
  ngFrameSize: 875,
  largeScreen: 992,
  xLargeScreen: 1200,
  xxLargeScreen: 1700,
};

// iterate through the sizes and create a media template
export const media = Object.keys(viewSizes).reduce((accumulator, label) => {
    const pxSize = viewSizes[label];
    accumulator[label] = (...args) => css`
    @media (min-width: ${pxSize}px) {
      ${css(...args)}
    }
  `;
    return accumulator
}, {});

