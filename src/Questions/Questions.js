import React from "react";
import styled from 'styled-components';
import Section from "./components/Section";

//link it with a xhr call when BE is ready
import QuestionSections from "../Data/mock.json";

const Questions = (props) => {
    return (
        <Wrapper>
            <Container>
                {QuestionSections.map((section)=>
                    <Section data={section}/>
                )}
            </Container>
        </Wrapper>
    )
};

export default Questions;

const Wrapper = styled.div`
    height: 100%;
    width: 100%;
    display: flex;
    justify-content: center;
    max-width:970px;
    margin: auto;
`;

const Container = styled.div`
    display: flex;
    background-color: rgb(255, 255, 255);
    width: 100%;
    flex: 1 1 0%;
    margin: 0 20px;
    max-width: 970px;
    flex-direction: column;
`;


