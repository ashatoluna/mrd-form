import React from "react";
import styled from 'styled-components';

const RichTextComponent = (props) => {

    return (
        <Wrapper>
            <Container>
                <RichTextElement>
                    <SectionName>{props.title}</SectionName>
                </RichTextElement>
            </Container>
        </Wrapper>
    )
};

export default RichTextComponent;

const Wrapper = styled.div`
    height: 100%;
    display: flex;
    justify-content: center;
`;

const Container = styled.div`
    display: flex;
    background-color: rgb(255, 255, 255);
    width: 100%;
    flex: 1 1 0%;
    margin: 0px auto;

    flex-direction: column;
`;

const RichTextElement = styled.div`
    border-bottom: 1px solid rgba(64, 142, 167, 0.3);
    padding: 16px 0px 50px;
    width:100%;
`;
const SectionName = styled.div`
    font-family: Roboto;
    font-size: 30px;
    font-weight:900;
    white-space: pre-wrap;
    color:#327e95;
    text-align: left;
`;

