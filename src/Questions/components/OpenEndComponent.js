import React from "react";
import styled from 'styled-components';

const OpenEndComponent = (props) => {
    return (
        <Wrapper>
            <Container>
                <OpenEndQuestion>
                    <Title>{props.question.questionTitle}</Title>
                    <TextAreaContainer>
                        <textarea className={'test'} maxLength={props.question.maxLength}
                                  minLength={props.question.minLength}
                                  placeholder={props.question.placeholder}></textarea>
                    </TextAreaContainer>
                </OpenEndQuestion>
            </Container>
        </Wrapper>
    )
};

export default OpenEndComponent;

const Wrapper = styled.div`
    width: 100%;
    display: flex;
    justify-content: center;
`;

const Container = styled.div`
    display: flex;
    background-color: rgb(255, 255, 255);
    width: 100%;
    flex: 1 1 0%;
    margin: 0px auto;
    flex-direction: column;
`;

const OpenEndQuestion = styled.div`
  border-bottom: 1px solid rgba(64, 142, 167, 0.3);
    padding: 16px 0px 50px;
`;

const Title = styled.div`
     width: 100%;
    margin-bottom: 0px;
    color: rgb(64, 142, 167);
    font-size: 25px;
    font-family: Roboto, Ariel;
    display: flex;
    flex-direction: row;
    justify-content: space-between;
    align-items: flex-start;
    font-weight: 300;
    letter-spacing: -0.5px;
    word-break: break-word;
    padding: 16px 0px 50px;
    text-align:left;
`;

const TextAreaContainer = styled.div`
    width:100%;
    .test {
        width: 100%;
        min-height: 100px;
        color: rgb(64, 142, 167);
        font-family: Roboto;
        font-size: 25px;
        resize: none;
        padding: 20px 20px 0px 0px;
        border-width: initial;
        border-style: none;
        border-color: initial;
        border-image: initial;
        outline: none;
        background: transparent;
        transition: all 0.5s ease 0s;
        overflow: hidden;      
    }
`;
