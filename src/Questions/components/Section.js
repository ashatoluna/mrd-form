import React from "react";
import styled from 'styled-components';
import RichTextComponent from './RichTextComponent';
import OpenEndComponent from './OpenEndComponent';


const Section = (props) => {
 const {sectionTitle, questions}= props.data;
    return (
        <Wrapper>
            <RichTextComponent title={sectionTitle}/>
            {questions.map((question)=>
                 <OpenEndComponent question={question}/>
            )}

        </Wrapper>
    )
};

export default Section;

const Wrapper = styled.div`
    width: 100%; 
    display: flex;
    justify-content: center;
    flex-direction: column;   
`;
