import React from "react";
import styled from "styled-components";
import {Scrollbars} from 'react-custom-scrollbars';
import Questions from '../Questions/Questions';

class PageContent extends React.Component {

    render() {
        return (
            <ContentWrapper>
                <Scrollbars autoHeight autoHeightMin="100%" autoHeightMax="100%"
                    style={scrollBarStyle}
                    renderTrackHorizontal={props => <div {...props} style={{display: 'none'}} className="track-horizontal"/>}
                    renderTrackVertical={props => <div {...props} className="track-vertical"/>}>
                        <Questions/>
                </Scrollbars>
            </ContentWrapper>
        )
    }
}

export default PageContent;

const scrollBarStyle = {
    width: '100%',
    height: '100%',
    position:'absolute'
};

const ContentWrapper = styled.div`
    display: flex;
    width: 100%;
    flex-grow: 1;
    position: relative;

    max-width: 970px;
    margin: auto;
    justify-content: center;
    .track-vertical {
      top: 2px;
      bottom: 2px;
      right: 2px;
      border-radius: 3px;
      background: rgba(255, 255, 255, 0.111);
}
`;



