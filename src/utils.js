import {viewSizes} from "./styles/utils";

export const isMobile = ()=>{
    const {window} = global;
    return window && (window.innerWidth > 0 && window.innerWidth <= viewSizes.mediumScreen);
};


