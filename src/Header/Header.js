import React from "react";
import styled from 'styled-components';
import Tolunalogo from '../assets/toluna-logo.png';
import {media} from "../styles/utils";

const Header = (props) => {
    return (
        <HeaderWrapper>
            <Wrapper>
                <HeaderText>Toluna Market Requirements Document LIGHT</HeaderText>
                <HeaderLogo> <img src={Tolunalogo} alt="logo"/></HeaderLogo>
            </Wrapper>
        </HeaderWrapper>
    )
};


export default Header;

const HeaderWrapper = styled.div`
    width: 100%;
    background-color: rgb(238, 238, 238);
`;

const Wrapper = styled.div`
    width: 100%;
    background-color: rgb(238, 238, 238);
    padding: 20px 0;
    display: flex;
    justify-content: center;
    justify-content: space-between;
    max-width:970px;
    margin:auto;
`;

const HeaderText = styled.div`
    color: rgb(64, 142, 167);
    font-size: 24px;
    font-family: Roboto, Ariel;
    font-weight: 300;
    ${media.mediumScreen`
        font-size: 26px;
    `}
`;

const HeaderLogo = styled.div`

`;