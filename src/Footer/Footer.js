import React from "react";
import styled from 'styled-components';
import {Button} from 'toluna-react-components';

const Footer = (props) => {
    return (
        <Wrapper>
            <ButtonWrapper>
                <Button className={'folder'}
                        onClick={() => alert('click')}
                        buttonStyle={Button.BUTTON_STYLE.RECTANGLE}
                        text="View in folder"
                />
                <Button className={'save'}
                        onClick={() => alert('click')}
                        buttonStyle={Button.BUTTON_STYLE.RECTANGLE}
                        iconName="check"
                        iconType="font-awesome"
                        text="Save"
                        primary
                />
            </ButtonWrapper>
        </Wrapper>
    )
};

const Wrapper = styled.div`
    border-top: 1px solid rgba(177, 177, 177, 0.4);
    padding: 20px;
    margin: 0;
    background: #fff;
    justify-content: space-around;
`;

const ButtonWrapper = styled.div`
    justify-content: space-between;
    max-width:970px;
    margin:auto;
    display:flex;
`;
export default Footer;
