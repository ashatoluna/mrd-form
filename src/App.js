import React from 'react';
import './App.css';
import Header from './Header/Header';
import PageContent from './Questions/PageContent';
import Footer from "./Footer/Footer";
import styled from 'styled-components';

function App() {
  return (
    <div className="App">
        <PageWrapper>
            <Header/>
                <PageContent/>
            <Footer/>
        </PageWrapper>
    </div>
  );
}

export default App;

const PageWrapper = styled.div`
    height: 100%;
    display: flex;
    flex-direction: column;
`;